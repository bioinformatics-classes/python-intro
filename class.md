### classes[7] = "A gentle intro to programming"

#### Perspetivas em bioinformática 2021-2022

![Logo EST](assets/logo-ESTB.png)

Francisco Pina Martins

[@FPinaMartins](https://twitter.com/FPinaMartins)

---

### Python

* &shy;<!-- .element: class="fragment" -->"General purpose" language
  * &shy;<!-- .element: class="fragment" -->Interpreted language
  * &shy;<!-- .element: class="fragment" -->Focused on code readability
  * &shy;<!-- .element: class="fragment" -->High level language
  * &shy;<!-- .element: class="fragment" -->Object-oriented

<img class="fragment" src="assets/python_logo.png" style="background:none; border:none; box-shadow:none;">

---

### Installing python

* &shy;<!-- .element: class="fragment" -->Python 3 is installed by default on most GNU/Linux distributions
* &shy;<!-- .element: class="fragment" -->How do we write python code?
* &shy;<!-- .element: class="fragment" -->How do we run python code?
  * &shy;<!-- .element: class="fragment" -->We need a code editor
  * &shy;<!-- .element: class="fragment" -->*Vim* and *Gedit* will do fine
  * &shy;<!-- .element: class="fragment" -->We can do better

|||

### Installing python

```bash
sudo apt update
sudo apt install idle3

# Alternative editor for starters
sudo apt install mu-editor
```

---

### Now live!

<img src="assets/now-live-icon.png" style="background:none; border:none; box-shadow:none;">

---

### Usefull resources

* [learnpython.org](https://www.learnpython.org/en/)
* [The python tutorial](https://docs.python.org/3/tutorial/index.html)
* [The python guru](https://thepythonguru.com/)
* [devmedia](https://www.devmedia.com.br/download-do-python-e-os-primeiros-passos/37003) (Em Português do Brasil)
* [Mak Summerfield](http://cs.stmarys.ca/~porter/csc/227/ProgrammingInPython3.pdf) (PDF Book)
* [Programming in Python 3](https://www.amazon.com/Programming-Python-Complete-Introduction-Language/dp/0321680561) (Paper book)
